

#!/bin/bash
#cek

#Menampilkan pesan untuk input password
echo -n "Masukkan password: "

#Membaca input lalu dimasukkan ke dalam variabel pswd
read pswd

#Memeriksa atau validasi kekuatan password dengan spesifikasi:
#	Minimum 8 karakter
#	Minimal terdapat huruf kecil, huruf besar, bilangan, dan simbol
#	Tidak sama dengan username dan sebagian/seluruh nama user

if [[ ${#pswd} -ge 8 && "$pswd" != `whoami` && "$pswd" =~ [a-z] && "$pswd" =~ [A-Z] && "$pswd" =~ [0-9] && "$pswd" =~ [^A-Za-z0-9] && ! `echo $pswd | grep "$USER"` ]] 
then

#Jika password memenuhi kriteria maka akan menampilkan pesan Good Password
	echo "Good Password"
else

#Jika password tidak memenuhi maka akan menampilkan pesan Weak Password
	echo "Weak Password"
fi

#Menampilkan password yang di-input
echo "Password yang kamu masukin: " $pswd 

